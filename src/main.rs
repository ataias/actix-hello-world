use actix_files::Files;
use actix_web::{middleware::Logger, web, App, HttpServer};

mod atomic;
mod errors;
mod handlers;
mod store;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "info");
    std::env::set_var("RUST_BACKTRACE", "1");
    env_logger::init();

    let data = store::AppState::new();

    HttpServer::new(move || {
        let logger = Logger::default();

        App::new()
            .wrap(logger)
            .app_data(web::Data::new(data.clone()))
            .configure(atomic::configure())
            .configure(handlers::configure())
            .configure(errors::configure())
            .service(Files::new("/static", ".").prefer_utf8(true))
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
