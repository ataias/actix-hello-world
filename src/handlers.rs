use actix_web::{
    body::BoxBody, http::header::ContentType, Error, HttpRequest, HttpResponse, Responder,
};
use actix_web::{get, web};
use futures::future::ok;
use futures::stream::once;
use serde::Serialize;

pub(super) fn configure() -> impl FnOnce(&mut web::ServiceConfig) {
    |config: &mut web::ServiceConfig| {
        config.service(web::scope("/handlers").service(json).service(stream));
    }
}

#[derive(Serialize)]
struct MyObj {
    name: &'static str,
}

impl Responder for MyObj {
    type Body = BoxBody;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        let body = serde_json::to_string(&self).unwrap();

        HttpResponse::Ok()
            .content_type(ContentType::json())
            .body(body)
    }
}

#[get("/json")]
async fn json() -> impl Responder {
    MyObj { name: "user" }
}

#[get("/stream")]
async fn stream() -> impl Responder {
    let body = once(ok::<_, Error>(web::Bytes::from_static(b"test")));

    HttpResponse::Ok()
        .content_type("application/json")
        .streaming(body)
}
