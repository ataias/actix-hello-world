use actix_files::NamedFile;
use actix_web::Responder;
use actix_web::{error, get, web};
use derive_more::{Display, Error};
use log::info;
use std::io;

pub(super) fn configure() -> impl FnOnce(&mut web::ServiceConfig) {
    |config: &mut web::ServiceConfig| {
        config.service(web::scope("/errors").service(file).service(custom));
    }
}

#[get("/file")]
async fn file() -> impl Responder {
    io::Result::Ok(NamedFile::open("static/index.html")?)
}

#[derive(Debug, Display, Error)]
#[display(fmt = "my error: {}", name)]
struct MyError {
    name: &'static str,
}

impl error::ResponseError for MyError {}

#[get("/custom")]
async fn custom() -> Result<&'static str, MyError> {
    let err = MyError { name: "test" };
    info!("{}", err);
    Err(err)
}
