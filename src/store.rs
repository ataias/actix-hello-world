use std::{cell::Cell, sync::atomic::AtomicUsize, sync::Arc};

#[derive(Clone)]
pub(super) struct AppState {
    pub local_count: Cell<usize>,
    pub global_count: Arc<AtomicUsize>,
}

impl AppState {
    pub fn new() -> Self {
        AppState {
            local_count: Cell::new(0),
            global_count: Arc::new(AtomicUsize::new(0)),
        }
    }
}
